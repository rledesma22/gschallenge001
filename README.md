
# Galicia Seguros Coding Challenge

¡Bienvenido!

Nos encontramos en la búsqueda de desarrolladores para que se incorporen a distintos equipos en GS. Si te sentís dispuesto a afrontar el desafío, tomate un rato para analizar claramente el problema y resolverlo.

### ¿Cómo participar del proceso?

En este README y en el TODO detallamos el problema a resolver. Te recomendamos que para la resolución realices un fork del repo
lo subas a un repositorio público, lo resuelvas y envies el link al email ******, adicionalmente, si tenés algún comentario sobre tu implementación, también podés contarnos ¡todo suma!

### El problema

El proyecto tiene un método que genera un reporte de formas geométricas. Éste procesa algunos datos para presentar información extra
como el cálculo de área o el perímetro.
La firma del método es la siguiente:

```csharp
public static string Print(IList<Figure> figures, int language)
```

Revisando el código, encontramos que es muy díficil poder agregar o una nueva forma geométrica, o imprimir el reporte en otro idioma. Por otra parte se verifica que la implementación es de difícil mantenimiento. ¿Nos podrías dar una mano con el refactor de la clase Figure? 
En la clase encontrarás un TODO con los requerimientos a satisfacer una vez completada la refactorización.

Acompañando al proyecto encontrarás una serie de tests unitarios (con la biblioteca XUnit) que describen el comportamiento del método Print. **Modifica el código de los tests como creas conveniente al refactor realizado, la condición es que los tests deben pasar correctamente al entregar la solución.** 
Incluye también nuevos tests unitarios para validar el comportamiento de la funcionalidad requerida.

#### ¡Expongamos la funcionalidad!

Adicional al refactor, te pedimos que la ejecución de la impresión se exponga en el endpoint de una Web API y
que implementes la aplicación en un contenedor docker.

### Cómo funciona

La solución (.sln) se realizó con .NET Core 3.1
Dentro de la misma te encontrás una la clase Figure y un pequeño proyecto con test unitarios sobre el método de impresión de reporte.

¡La resolución es libre y cómo encarar el problema queda a criterio de quien lo resuelva!

**¡¡Good Luck!!**

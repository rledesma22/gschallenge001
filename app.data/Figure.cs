﻿/*
 * Realizar refactor respetando principios de programación orientada a objetos. 
 * ¿Qué sucede si es necesario soportar un nuevo idioma para los reportes o agregar más formas geométricas?
 *
 * Cualquier cambio es permitido tanto en el código como en los tests. La única condición es que los tests pasen OK.
 *
 * TODO: Implementar Pentágono/Rectangulo, agregar otro idioma a reporting.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace app.data
{
    public class Figure
    {
        public const int Square = 1;
        public const int Triangle = 2;
        public const int Circle = 3;

        public const int Spanish = 1;
        public const int English = 2;
        
        private readonly decimal _side;

        public int Type { get; set; }
        
        public Figure(int type, decimal lengh)
        {
            Type = type;
            _side = lengh;
        }

        public static string Print(IList<Figure> figures, int language)
        {
            var sb = new StringBuilder();

            if (!figures.Any()) sb.Append(language == Spanish ? "Lista vacía de formas!" : "Empty list of shapes!");
            else
            {
                sb.Append(language == Spanish ? "Reporte de Formas " : "Shapes report ");

                var squareQuantity = 0;
                var circleQuantity = 0;
                var triangleQuantity = 0;

                var squaresArea = 0m;
                var circlesArea = 0m;
                var trianglesArea = 0m;

                var squaresPerimeter = 0m;
                var circlesPerimeter = 0m;
                var trianglesPerimeter = 0m;

                for (var i = 0; i < figures.Count; i++)
                {
                    if (figures[i].Type == Square)
                    {
                        squareQuantity++;
                        squaresArea += figures[i].CalculateArea();
                        squaresPerimeter += figures[i].CalculatePerimeter();
                    }

                    if (figures[i].Type == Triangle)
                    {
                        triangleQuantity++;
                        trianglesArea += figures[i].CalculateArea();
                        trianglesPerimeter += figures[i].CalculatePerimeter();
                    }

                    if (figures[i].Type == Circle)
                    {
                        circleQuantity++;
                        circlesArea += figures[i].CalculateArea();
                        circlesPerimeter += figures[i].CalculatePerimeter();
                    }
                }

                sb.Append(GetTranslation(squareQuantity, squaresArea, squaresPerimeter, Square, language));
                sb.Append(GetTranslation(circleQuantity, circlesArea, circlesPerimeter, Circle, language));
                sb.Append(GetTranslation(triangleQuantity, trianglesArea, trianglesPerimeter, Triangle, language));

                sb.Append("TOTAL: ");
                sb.Append(squareQuantity + circleQuantity + triangleQuantity + " " + (language == Spanish ? "formas" : "shapes") + " ");
                sb.Append((language == Spanish ? "Perimetro " : "Perimeter ") +
                          (squaresPerimeter + trianglesPerimeter + circlesPerimeter).ToString("#.##") + " ");
                sb.Append("Area " + (squaresArea + circlesArea + trianglesArea).ToString("#.##"));
            }

            return sb.ToString();
        }

        private static string GetTranslation(int quantity, decimal area, decimal perimeter, int type, int language)
        {
            if (quantity <= 0) return string.Empty;

            return language == Spanish
                ? $"{quantity} {TranslateFigure(type, quantity, language)} | Area {area:#.##} | Perimetro {perimeter:#.##} "
                : $"{quantity} {TranslateFigure(type, quantity, language)} | Area {area:#.##} | Perimeter {perimeter:#.##} ";
        }

        private static string TranslateFigure(int type, int quantity, int language)
        {
            switch (type)
            {
                case Square:
                    if (language == Spanish) return quantity == 1 ? "Cuadrado" : "Cuadrados";
                    else return quantity == 1 ? "Square" : "Squares";
                case Circle:
                    if (language == Spanish) return quantity == 1 ? "Círculo" : "Círculos";
                    else return quantity == 1 ? "Circle" : "Circles";
                case Triangle:
                    if (language == Spanish) return quantity == 1 ? "Triángulo" : "Triángulos";
                    else return quantity == 1 ? "Triangle" : "Triangles";
            }

            return string.Empty;
        }

        public decimal CalculateArea()
        {
            switch (Type)
            {
                case Square: return _side * _side;
                case Circle: return (decimal) Math.PI * (_side / 2) * (_side / 2);
                case Triangle: return ((decimal) Math.Sqrt(3) / 4) * _side * _side;
                default:
                    throw new ArgumentOutOfRangeException($@"Forma desconocida");
            }
        }

        public decimal CalculatePerimeter()
        {
            switch (Type)
            {
                case Square: return _side * 4;
                case Circle: return (decimal) Math.PI * _side;
                case Triangle: return _side * 3;
                default:
                    throw new ArgumentOutOfRangeException($@"Forma desconocida");
            }
        }
    }
}
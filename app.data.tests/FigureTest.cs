﻿using System.Collections.Generic;
using Xunit;

namespace app.data.tests
{
    public class FigureTest
    {
        [Fact]
        public void TestPrintEmptyList()
        {
            Assert.Equal("Lista vacía de formas!",
                Figure.Print(new List<Figure>(), 1));
        }

        [Fact]
        public void TestPrintEmptyListEnglishLanguage()
        {
            Assert.Equal("Empty list of shapes!",
                Figure.Print(new List<Figure>(), 2));
        }

        [Fact]
        public void TestPrintSquare()
        {
            var squares = new List<Figure> {new Figure(Figure.Square, 5)};

            var result = Figure.Print(squares, Figure.Spanish);

            Assert.Equal("Reporte de Formas 1 Cuadrado | Area 25 | Perimetro 20 TOTAL: 1 formas Perimetro 20 Area 25", result);
        }

        [Fact]
        public void TestPrintSquaresEnglishLanguage()
        {
            var squares = new List<Figure>
            {
                new Figure(Figure.Square, 5),
                new Figure(Figure.Square, 1),
                new Figure(Figure.Square, 3)
            };

            var result = Figure.Print(squares, Figure.English);

            Assert.Equal("Shapes report 3 Squares | Area 35 | Perimeter 36 TOTAL: 3 shapes Perimeter 36 Area 35", result);
        }

        [Fact]
        public void TestPrintMixFigureTypesEnglishLanguage()
        {
            var figures = new List<Figure>
            {
                new Figure(Figure.Square, 5),
                new Figure(Figure.Circle, 3),
                new Figure(Figure.Triangle, 4),
                new Figure(Figure.Square, 2),
                new Figure(Figure.Triangle, 9),
                new Figure(Figure.Circle, 2.75m),
                new Figure(Figure.Triangle, 4.2m)
            };

            var response = Figure.Print(figures, Figure.English);

            Assert.Equal(
                "Shapes report 2 Squares | Area 29 | Perimeter 28 2 Circles | Area 13.01 | Perimeter 18.06 3 Triangles | Area 49.64 |" +
                " Perimeter 51.6 TOTAL: 7 shapes Perimeter 97.66 Area 91.65",
                response);
        }

        [Fact]
        public void TestPrintMixTypesEnglishLanguage()
        {
            var figures = new List<Figure>
            {
                new Figure(Figure.Square, 5),
                new Figure(Figure.Circle, 3),
                new Figure(Figure.Triangle, 4),
                new Figure(Figure.Square, 2),
                new Figure(Figure.Triangle, 9),
                new Figure(Figure.Circle, 2.75m),
                new Figure(Figure.Triangle, 4.2m)
            };

            var response = Figure.Print(figures, Figure.Spanish);

            Assert.Equal(
                "Reporte de Formas 2 Cuadrados | Area 29 | Perimetro 28 2 Círculos | Area 13.01 | Perimetro 18.06 3 Triángulos | Area 49.64 |" +
                " Perimetro 51.6 TOTAL: 7 formas Perimetro 97.66 Area 91.65",
                response);
        }
    }
}